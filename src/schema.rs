table! {
    posts (id) {
        id -> Int4,
        title -> Text,
        body -> Text,
        date -> Date,
    }
}

table! {
    roles (role_id) {
        role_id -> Int4,
        rolename -> Varchar,
    }
}

table! {
    users (user_id) {
        user_id -> Int4,
        username -> Varchar,
        email -> Nullable<Varchar>,
        password -> Varchar,
        salt -> Varchar,
        role_id -> Int4,
    }
}

joinable!(users -> roles (role_id));

allow_tables_to_appear_in_same_query!(
    posts,
    roles,
    users,
);
