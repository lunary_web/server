use db::*;
use diesel::prelude::*;
use log::*;
use models::post::{NewPost, Post};
use models::user::User;
use rocket::request::Form;
use rocket::response::Redirect;
use rocket::Route;
use rocket_contrib::templates::Template;
use tera::Context;

use std::path::{Path, PathBuf};

#[get("/", rank = 8)]
fn blog_redirect() -> Redirect {
    Redirect::to("/login")
}

#[get("/<any..>", rank = 9)]
fn blog_sub_redirect(any: PathBuf) -> Redirect {
    Redirect::to("/login")
}

#[get("/page/<page_num>")]
fn blog_page(connection: DbConn, user: User, page_num: u32) -> Template {
    warn!("ooh nooo");
    use diesel::dsl::count_star;
    use schema::posts::dsl::*;

    let posts_per_page = 10;
    let page_num_shift = 2;

    let posts_offset = (page_num - 1) as i64 * posts_per_page;
    let posts_list = posts
        .order(date)
        .offset(posts_offset)
        .limit(posts_per_page)
        .load::<Post>(&*connection)
        .expect("WTF");

    let num_of_posts: i64 = posts.select(count_star()).first(&*connection).expect("WTF");
    let num_of_pages = 1 + (num_of_posts / posts_per_page) as u32;

    let (left_pages_border, right_pages_border) = {
        let (mut lpb, mut rpb) = (
            page_num.saturating_sub(page_num_shift),
            page_num + page_num_shift + 1,
        );
        if 1 > lpb {
            lpb = 1;
        }
        if num_of_pages < rpb {
            rpb = num_of_pages + 1;
        }
        (lpb, rpb)
    };

    let mut context = Context::new();
    context.insert("articles", &posts_list);
    context.insert("current_page", &page_num);
    context.insert("num_of_pages", &num_of_pages);
    context.insert(
        "pages",
        &(left_pages_border..right_pages_border).collect::<Vec<u32>>(),
    );
    Template::render("blog", &context)
}

#[get("/")]
fn blog(connection: DbConn, user: User) -> Template {
    blog_page(connection, user, 1)
}

#[get("/post/<blog_id>")]
fn post(connection: DbConn, user: User, blog_id: i32) -> Template {
    use schema::posts::dsl::*;

    let blog = posts
        .filter(id.eq(blog_id))
        .load::<Post>(&*connection)
        .expect("WTF");

    let mut context = Context::new();
    context.insert("blog", &blog[0]);
    Template::render("post", &context)
}

#[get("/post/add")]
fn new_post(user: User) -> Template {
    let context = Context::new();
    Template::render("post_add", &context)
}

use schema::posts::dsl::*;

#[post("/post/add", data = "<new_post>")]
fn process_new_post(connection: DbConn, user: User, new_post: Form<NewPost>) -> Template {
    println!("new post is {:?}", new_post);
    // use schema::posts::dsl::*;
    use schema::posts;

    let my_post: Post = diesel::insert_into(posts::table)
        .values(new_post.0)
        .get_result(&*connection)
        .expect("WTF");
    println!("my res = {:?}", my_post);

    let posts_list = posts.load::<Post>(&*connection).expect("WTF");

    let mut context = Context::new();
    context.insert("articles", &posts_list);

    Template::render("blog", &context)
}

pub fn routes() -> Vec<Route> {
    routes![
        new_post,
        process_new_post,
        post,
        blog,
        blog_page,
        blog_redirect,
        blog_sub_redirect
    ]
}
