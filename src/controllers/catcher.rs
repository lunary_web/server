use rocket::Catcher;

#[catch(404)]
fn not_found() -> String {
    String::from("404. WTF. Again")
}

#[catch(500)]
fn internal_error() -> String {
    String::from("Wtf!!! Probably I fucked up")
}

pub fn routes() -> Vec<Catcher> {
    catchers![not_found, internal_error]
}
