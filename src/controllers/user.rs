use argon2rs::verifier::Encoded;
use db::*;
use diesel::prelude::*;
use log::*;
use models::user::User;
use rocket::http::{Cookie, Cookies};
use rocket::request::Form;
use rocket::response::Redirect;
use rocket::Route;
use rocket_contrib::templates::Template;

use tera::Context;

#[derive(FromForm, Debug)]
pub struct Credentials {
    login: String,
    password: String,
}

#[get("/login", rank = 1)]
fn login_redirect(user: User) -> Redirect {
    Redirect::to("/")
}

#[get("/login", rank = 2)]
fn login() -> Template {
    let context = Context::new();
    Template::render("login", &context)
}

#[post("/login", data = "<creds>", rank = 2)]
fn process_login(connection: DbConn, mut session: Cookies, creds: Form<Credentials>) -> Redirect {
    use schema::users::dsl::*;

    let user = users
        .filter(username.eq(&creds.login))
        .first::<User>(&*connection);

    let user: User = match user {
        Err(e) => {
            debug!("user error: {}", e);
            return Redirect::to("/login");
        }
        Ok(u) => u,
    };

    let enc = Encoded::from_u8(user.password.as_bytes()).unwrap();
    if enc.verify(creds.password.as_bytes()) {
        session.add_private(Cookie::new("user_id", user.user_id.to_string()));
        Redirect::to("/")
    } else {
        Redirect::to("/login")
    }
}

#[derive(FromForm, Debug)]
pub struct RegisterData {
    login: String,
    email: Option<String>,
    password: String,
}

#[get("/register")]
fn register() -> Template {
    let context = Context::new();
    Template::render("register", &context)
}

#[post("/register", data = "<regdata>", rank = 2)]
fn process_register(connection: DbConn, user: User, regdata: Form<RegisterData>) -> Redirect {
    use schema::users::dsl::*;

    Redirect::to("/")
}

pub fn routes() -> Vec<Route> {
    routes![
        login,
        process_login,
        login_redirect,
        register,
        process_register
    ]
}
