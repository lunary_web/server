use models::user::User;
use rocket::response::NamedFile;
use rocket::response::Redirect;
use rocket::Route;
use rocket_contrib::templates::Template;
use std::path::{Path, PathBuf};
use tera::Context;

#[get("/")]
fn user_index(user: User) -> Template {
    Template::render("index", &Context::new())
}

#[get("/", rank = 2)]
fn index() -> Redirect {
    Redirect::to("/login")
}

// #[get("/<file..>", rank = 10)]
// fn static_files(file: PathBuf) -> Option<NamedFile> {
//     NamedFile::open(Path::new("/srv/www/").join(file)).ok()
// }

pub fn routes() -> Vec<Route> {
    routes![index, user_index]
}
