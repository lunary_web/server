#![feature(plugin)]
#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate diesel;
extern crate chrono;
extern crate dotenv;
extern crate failure;
extern crate fern;
extern crate log;
extern crate r2d2;

#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

extern crate serde;
extern crate tera;
#[macro_use]
extern crate serde_derive;
extern crate argon2rs;

pub mod controllers;
pub mod db;
pub mod models;
pub mod schema;

use controllers::base::*;
use controllers::blog;
use controllers::catcher::*;
use controllers::*;
use db::*;
use failure::Error;
use log::*;
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;

pub fn set_log_level(lvl: u64) -> Result<(), Error> {
    use fern::colors::{Color, ColoredLevelConfig};

    let log_level = match lvl {
        0 => log::LevelFilter::Off,
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 | _ => log::LevelFilter::Trace,
    };

    let colors_line = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::White)
        .debug(Color::White)
        .trace(Color::BrightMagenta);
    let colors_level = colors_line.clone().info(Color::Green);

    fern::Dispatch::new()
        .format(move |out, message, record| {
            if record.target() == "server" {
                out.finish(format_args!(
                    "{color_line}[{target}][{level}{color_line}] {message}\x1B[0m",
                    color_line = format_args!(
                        "\x1B[{}m",
                        colors_line.get_color(&record.level()).to_fg_str()
                    ),
                    target = record.target(),
                    level = colors_level.color(record.level()),
                    message = message
                ));
            }
        })
        .level(log_level)
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}

fn main() {
    // set_log_level(5);

    let db_pool = create_db_pool();
    warn!("ooh nooo");

    ::rocket::ignite()
        .attach(Template::fairing())
        .mount("/", StaticFiles::from("/srv/www"))
        .mount("/", base::routes())
        .mount("/", user::routes())
        .mount("/blog", blog::routes())
        .manage(db_pool)
        .register(catcher::routes())
        .launch();
}
