use db::*;
use diesel::deserialize::Queryable;
use diesel::prelude::*;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request};
use schema::{roles, users};

#[derive(Queryable, Debug, Serialize, PartialEq)]
pub struct User {
    pub user_id: i32,
    pub username: String,
    pub email: Option<String>,
    pub password: String,
    pub salt: String,
    pub role_id: i32,
}

impl<'a, 'r> FromRequest<'a, 'r> for User {
    type Error = ();

    fn from_request(req: &'a Request<'r>) -> request::Outcome<User, ()> {
        use schema::users::dsl::*;
        let connection = req.guard::<DbConn>()?;

        let user = req
            .cookies()
            .get_private("user_id")
            .and_then(|cookie| cookie.value().parse::<i32>().ok())
            .map(|uid| users.filter(user_id.eq(&uid)).first::<User>(&*connection));

        let user = match user {
            None => return Outcome::Forward(()),
            Some(u) => u,
        };
        match user {
            Err(_) => {
                return Outcome::Failure((::rocket::http::Status::NotFound, ()));
            }
            Ok(u) => Outcome::Success(u),
        }
    }
}
