use schema::posts;

use chrono::naive::NaiveDate;

#[derive(Queryable, Debug, Serialize)]
pub struct Post {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub date: NaiveDate,
}

#[derive(Insertable, Debug, FromForm)]
#[table_name = "posts"]
pub struct NewPost {
    pub title: String,
    pub body: String,
}
