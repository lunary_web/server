CREATE TABLE "posts" (
	"id" serial NOT NULL UNIQUE,
	"title" TEXT NOT NULL,
	"body" TEXT NOT NULL,
	"date" DATE NOT NULL DEFAULT current_date,
	CONSTRAINT posts_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);
