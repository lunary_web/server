-- Your SQL goes here

CREATE TABLE "users" (
	"user_id" serial NOT NULL UNIQUE,
	"username" VARCHAR(32) NOT NULL UNIQUE,
	"email" VARCHAR(128),
	"password" VARCHAR(512) NOT NULL,
	"salt" VARCHAR(64) NOT NULL UNIQUE,
	"role_id" integer NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY ("user_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "roles" (
	"role_id" serial NOT NULL UNIQUE,
	"rolename" VARCHAR(30) NOT NULL UNIQUE,
	CONSTRAINT roles_pk PRIMARY KEY ("role_id")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "users" ADD CONSTRAINT "users_fk0" FOREIGN KEY ("role_id") REFERENCES "roles"("role_id");
