-- This file should undo anything in `up.sql`

ALTER TABLE "users" DROP CONSTRAINT IF EXISTS "users_fk0";

DROP TABLE IF EXISTS "users";

DROP TABLE IF EXISTS "roles";
