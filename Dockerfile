FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
	curl \
	vim \
	iproute2 \
	libpq5 \
	&& rm -rf /var/lib/apt/lists/*

COPY target/release/server /usr/local/bin/
COPY Rocket.toml /etc/

WORKDIR /etc

EXPOSE 1337
CMD ["server"]
